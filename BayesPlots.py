"""
BayesPlots: Plots the posterior distribution for a selected model or a list of models (TempoNest dependent structure)

__author__ = "Aditya Parthasarathy"
__copyright__ = "Copyright 2019, YoungPulsarTiming"
__license__ = "Public Domain"
__version__ = "0.2"
__maintainer__ = "Aditya Parthasarathy"
__email__ = "adityapartha3112@gmail.com"
__status__ = "Development"

"""

import numpy as np
import pandas as pd
import argparse
import matplotlib.pyplot as plt
import sys
import os
import glob
import re
import math
from chainconsumer import ChainConsumer
from astropy import units as u
from astropy.coordinates import Angle

parser=argparse.ArgumentParser(description="TempoNest posteriors")
parser.add_argument("-res",dest="results",help="Path to a particular temponest results directory")
parser.add_argument("-name", dest="name", help="Name of the output pdf plot file", action='store_true')
parser.add_argument("-bestmodels", dest="bestmodels", help="Path to the file containing the best models")
args=parser.parse_args()

if args.bestmodels:
    bestmodel = np.loadtxt(str(args.bestmodels),dtype=str)
    for num,psr in enumerate(bestmodel[:,0]):
        psrname = os.path.split(psr)[1]
        print psrname
        if not os.path.exists("forpaper/posteriors_updated/{0}.pdf".format(psrname)):
            dir = os.path.join(psr,bestmodel[num,1])
            dirpath,dirname = os.path.split(dir)
            paramnames = glob.glob(os.path.join(dir,"*.paramnames"))[0]
            names = pd.read_table(paramnames,delim_whitespace=True,names=("sno","param"),dtype={'sno': np.str, 'param': np.str})

            posterior_path = glob.glob(os.path.join(dir,"*-post_equal_weights.dat"))[0]
            cnames = names['param'].tolist()
            cnames.append("Likelihood")
            posteriors = pd.read_csv(posterior_path, names=cnames, delim_whitespace=True)

            #Rescaling
            rescale_file = glob.glob(os.path.join(dir,"*-T2scaling.txt"))[0]
            rescale_vals = pd.read_table(rescale_file,delim_whitespace=True,names=("param","units","mean","std"),dtype={'param':np.str, 'units':np.str, 'mean':np.str, 'std':np.str})
            for name in rescale_vals['param'].values:
                posteriors[name] = float(rescale_vals.loc[rescale_vals['param'] == name, 'mean']) + float(rescale_vals.loc[rescale_vals['param'] == name, 'std'])*posteriors[name]



            #Saving the posterior values
            for name in cnames:
                upper = np.percentile(posteriors[name],97.5) - np.percentile(posteriors[name],50)
                lower = np.percentile(posteriors[name],50) - np.percentile(posteriors[name],2.5)
                mean = np.percentile(posteriors[name],50)

                print name,mean,upper,lower

            c = ChainConsumer()

            #Removing additional parameters
            if psrname == "J1843-0355":
                pass
            elif psrname == "J1853+0011":
                pass
            else:
                remove_params = ["RAJ","DECJ","F0","F1","EFAC1","EQUAD1","Likelihood"]
                for element in remove_params:
                    cnames.remove(element)
                    posteriors = posteriors.drop(columns=element)

                posteriors["RedSlope"] = -posteriors["RedSlope"]

            #Renaming the parameters
            new_cnames = []
            for element in cnames:
                if element == "PMRA":
                    new_cnames.append(r"$\mu_{\alpha}$")
                elif element == "PMDEC":
                    new_cnames.append(r"$\mu_{\delta}$")
                elif element == "RedAmp":
                    new_cnames.append(r"$\log_{\rm 10}(A_{\rm red})$")
                elif element == "RedSlope":
                    new_cnames.append(r"$\beta$")
                elif element == "F2":
                    new_cnames.append(r"$\ddot{\nu}$")
                elif element == "RedCorner":
                    new_cnames.append(r"$f_{\rm c}$")
                elif element == "sineAmp":
                    new_cnames.append(r"$A_{\rm sin}$")
                elif element == "sinePhase":
                    new_cnames.append(r"$\phi_{\rm sin}$")
                elif element == "sineFreq":
                    new_cnames.append(r"$\nu_{\rm sin}$")
                elif element == "Likelihood":
                    new_cnames.append(r"$\mathcal{L}$")
                elif element == "RAJ":
                    new_cnames.append(r"$\alpha$")
                elif element == "DECJ":
                    new_cnames.append(r"$\delta$")
                elif element == "F0":
                    new_cnames.append(r"$\nu$")
                elif element == "F1":
                    new_cnames.append(r"$\dot{\nu}$")
                else:
                    new_cnames.append(element)


            c.add_chain(posteriors.values,parameters=new_cnames,bins=40,name="{0}".format(psrname))
            c.configure(diagonal_tick_labels=False,flip=False,sigma2d=True,kde=False,sigmas=[1.19,2.5],shade=True,shade_alpha=0.1,label_font_size=20,tick_font_size=20,max_ticks=3,summary=False,colors="#2c3e50",plot_color_params=True,contour_labels="confidence",summary_area=0.95)
            c.plotter.plot(filename="forpaper/posteriors_updated/{0}.pdf".format(psrname),figsize="PAGE",legend=False)
         

elif args.results:
            
    dir = os.path.join(args.results)
    dirpath,dirname = os.path.split(dir)
    psrpath,psrname = os.path.split(dirpath)
    paramnames = glob.glob(os.path.join(dir,"*.paramnames"))[0]
    names = pd.read_table(paramnames,delim_whitespace=True,names=("sno","param"),dtype={'sno': np.str, 'param': np.str})

    posterior_path = glob.glob(os.path.join(dir,"*-post_equal_weights.dat"))[0]
    cnames = names['param'].tolist()
    cnames.append("Likelihood")
    posteriors = pd.read_csv(posterior_path, names=cnames, delim_whitespace=True)

    #Rescaling
    rescale_file = glob.glob(os.path.join(dir,"*-T2scaling.txt"))[0]
    rescale_vals = pd.read_table(rescale_file,delim_whitespace=True,names=("param","units","mean","std"),dtype={'param':np.str, 'units':np.str, 'mean':np.str, 'std':np.str})
    for name in rescale_vals['param'].values:
        if name in cnames:
            posteriors[name] = float(rescale_vals.loc[rescale_vals['param'] == name, 'mean']) + float(rescale_vals.loc[rescale_vals['param'] == name, 'std'])*posteriors[name]


    #Saving the posterior values
    for name in cnames:
        upper = np.percentile(posteriors[name],97.5) - np.percentile(posteriors[name],50)
        lower = np.percentile(posteriors[name],50) - np.percentile(posteriors[name],2.5)
        mean = np.percentile(posteriors[name],50)

        print name,mean,upper,lower

    c = ChainConsumer()

    #Removing additional parameters
    remove_params = ["RAJ","DECJ","F0","F1","EFAC1","EQUAD1","Likelihood"]
    for element in remove_params:
        if element in cnames:
            cnames.remove(element)
            posteriors = posteriors.drop(columns=element)

    #Renaming the parameters
    new_cnames = []
    for element in cnames:
        if element == "PMRA":
            new_cnames.append(r"$\mu_{\alpha}$")
        elif element == "PMDEC":
            new_cnames.append(r"$\mu_{\delta}$")
        elif element == "RedAmp":
            new_cnames.append(r"$\log_{\rm 10}(A_{\rm red})$")
        elif element == "RedSlope":
            new_cnames.append(r"$\beta$")
        elif element == "F2":
            new_cnames.append(r"$\ddot{\nu}$")
        elif element == "RedCorner":
            new_cnames.append(r"$f_{\rm c}$")
        elif element == "sineAmp":
            new_cnames.append(r"$A_{\rm sin}$")
        elif element == "sinePhase":
            new_cnames.append(r"$\phi_{\rm sin}$")
        elif element == "sineFreq":
            new_cnames.append(r"$\nu_{\rm sin}$")
        elif element == "Likelihood":
            new_cnames.append(r"$\mathcal{L}$")
        elif element == "RAJ":
            new_cnames.append(r"$\alpha$")
        elif element == "DECJ":
            new_cnames.append(r"$\delta$")
        elif element == "F0":
            new_cnames.append(r"$\nu$")
        elif element == "F1":
            new_cnames.append(r"$\dot{\nu}$")
        else:
            new_cnames.append(element)


    c.add_chain(posteriors.values,parameters=new_cnames,bins=40,name="test")
    c.configure(diagonal_tick_labels=False,flip=False,sigma2d=True,kde=False,sigmas=[1.19,2.5],shade=True,shade_alpha=0.1,label_font_size=20,tick_font_size=20,max_ticks=3,summary=False,colors="#2c3e50",plot_color_params=True,contour_labels="confidence",summary_area=0.95)
    c.plotter.plot(filename="forpaper/test.pdf",figsize="PAGE",legend=True)
    plt.show()

