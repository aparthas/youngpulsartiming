# YoungPulsarTiming

A repository containing online material relevant to Parthasarathy et al. 2019a,b. 
Contains the YoungPulsarTiming pipeline using TempoNest and the posterior distribution plots for the timing models stated in the paper.


