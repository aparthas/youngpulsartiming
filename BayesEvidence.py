"""
BayesEvidence: Queries the Bayesian log-evidence for each timing model for the pulsar and displays the list of likely models depending upon the Bayes factor threshold. 

__author__ = "Aditya Parthasarathy"
__copyright__ = "Copyright 2019, YoungPulsarTiming"
__license__ = "Public Domain"
__version__ = "0.2"
__maintainer__ = "Aditya Parthasarathy"
__email__ = "adityapartha3112@gmail.com"
__status__ = "Development"

"""

import numpy as np
import glob
import os
import sys
import argparse
import pandas as pd

parser=argparse.ArgumentParser(description="TempoNest model evidences")
parser.add_argument("-psr",dest="pulsar",help="Path to a particular pulsar directory")
parser.add_argument("-all",dest="all_psrs", help="Path to the directory containing all pulsars")
args=parser.parse_args()


if args.pulsar:
    dirs = sorted(glob.glob(os.path.join(args.pulsar,"results*")))
    model_list = []
    evidence_list = []
    evidence_err_list = []
    bayes_factors = []
    likelymodels = []
    toggle = False #To check if atleast one model has BF > 5
    toggle1 = False #Toc check if atleast one model has BF > 1

    #Appending all the evidence and model names
    for dir in dirs:
        dirpath,dirname = os.path.split(dir)
        stats = glob.glob(os.path.join(dir,"*-stats.dat"))[0]
        with open(stats) as f:
            for line in f:
                sline = line.split()
                if len(sline) > 0:
                    if sline[1] == "Importance":
                        evidence = round(float(sline[5]),3)
                        evidence_err = round(float(sline[7]),4)
                    else:
                        pass
                else:
                    pass
        f.close()
        if dirname == "results_bp_noise_pl":
            basis_evidence = evidence
            basis_ev_err = evidence_err

        model_list.append(dirname)
        evidence_list.append(evidence)
        evidence_err_list.append(evidence_err)


    #Computing Bayes Factors
    for num in range(len(model_list)):
        bayes_factors.append(evidence_list[num]-basis_evidence)
        print model_list[num], bayes_factors[num], evidence_list[num]

    #Choosing the most likely models
    print "############# LIKELY MODELS ARE #############"
    for num in range(len(bayes_factors)):
        if bayes_factors[num] >= 3.0:
            toggle = True
            likelymodels.append(float(bayes_factors[num]))
            print model_list[num], bayes_factors[num], evidence_list[num]
        else:
            likelymodels.append(0.0)

    #If there are no models with BF>threshold
    if toggle == True:
        pass
    else:
        print "############ NO MODEL WITH BF>3. MODELS WITH POSITIVE(OR ZERO) BF ARE ###############"
        likelymodels = []
        for num in range(len(bayes_factors)):
            if bayes_factors[num] >= 0:
                toggle1 = True
                likelymodels.append(float(bayes_factors[num]))
                print model_list[num], bayes_factors[num], evidence_list[num]
            else:
                likelymodels.append(0.0)

    #Choosing the best model
    index =  likelymodels.index(max(likelymodels))
    print "############# MaxEvidence MODEL IS #############"
    print model_list[index], evidence_list[index], bayes_factors[index]

elif args.all_psrs:

    if not os.path.exists(os.path.join(args.all_psrs,"Evidence_database.pkl")):
        psrs = sorted(glob.glob(os.path.join(args.all_psrs,"J*")))
        count=0
        for psr in psrs:
            psrpath,psrname = os.path.split(psr)
            if not psrname == "J1513-5908":
                print psrname
                dirs = sorted(glob.glob(os.path.join(psr,"results*")))
                model_list = []
                evidence_list = []
                evidence_err_list = []
                bayes_factors = []
                flag_5 = []
                flag_pos = []
                psr_list = []

                #Appending all the evidence and model names
                for dir in dirs:
                    dirpath,dirname = os.path.split(dir)
                    stats = glob.glob(os.path.join(dir,"*-stats.dat"))[0]
                    with open(stats) as f:
                        for line in f:
                            sline = line.split()
                            if len(sline) > 0:
                                if sline[1] == "Importance":
                                    evidence = round(float(sline[5]),3)
                                    evidence_err = round(float(sline[7]),4)
                                else:
                                    pass
                            else:
                                pass
                    f.close()
                    if dirname == "results_bp_noise_pl":
                        basis_evidence = evidence
                        basis_ev_err = evidence_err

                    model_list.append(dirname)
                    evidence_list.append(evidence)
                    evidence_err_list.append(evidence_err)
                    psr_list.append(psr)


                #Computing Bayes Factors
                for num in range(len(model_list)):
                    bayes_factors.append(evidence_list[num]-basis_evidence)

                #Flag for models with BF>5
                for num in range(len(bayes_factors)):
                    if bayes_factors[num] >= 5.0:
                        flag_5.append(1)
                    else:
                        flag_5.append(0)

                #Flag for models with positive BF
                for num in range(len(bayes_factors)):
                    if bayes_factors[num] >= 0:
                        flag_pos.append(1)
                    else:
                        flag_pos.append(0)


                #for num in range(len(model_list)):
                #    print model_list[num],evidence_list[num],bayes_factors[num],flag_5[num],flag_pos[num]

                data = {'pulsar':psr_list,'model':model_list,'evidence':evidence_list,'bayesfactor':bayes_factors,'above5':flag_5,'positive':flag_pos}
                df = pd.DataFrame(data)
                df = df.astype({"pulsar": str, "model": str, "evidence":float, "bayesfactor":float, "above5":int, "positive":int})

                if count == 0:
                    df_all = df
                else:
                    df_all = df_all.append(df,ignore_index=True)

                count+=1

        df_all.to_pickle(os.path.join(args.all_psrs,"Evidence_database.pkl"))
        print ("Dataframe containing evidence values for all pulsars saved: {0}".format(args.all_psrs))

    else:
        print ("Loading existing evidence database from {0}".format(args.all_psrs))
        df_all = pd.read_pickle(os.path.join(args.all_psrs,"Evidence_database.pkl"))
        psrs = df_all.pulsar.unique()
        for index, row in df_all.iterrows():
            if row['above5'] == 1:
                print row['pulsar'], row['model'], row['bayesfactor'], row['evidence']




