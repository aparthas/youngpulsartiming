"""
GenerateResults: Code to reproduce the plots and tables in Paper-1. 

__author__ = "Aditya Parthasarathy"
__copyright__ = "Copyright 2019, YoungPulsarTiming"
__license__ = "Public Domain"
__version__ = "0.2"
__maintainer__ = "Aditya Parthasarathy"
__email__ = "adityapartha3112@gmail.com"
__status__ = "Development"

"""

import numpy as np
import argparse
import os
import sys
import pandas as pd
import glob
from astropy import units as u
from astropy.coordinates import Angle
from uncertainties import ufloat
import uncertainties
from astropy.stats import LombScargle
import matplotlib.pyplot as plt
from chainconsumer import ChainConsumer
import shlex
import subprocess
from scipy import stats
import math
import psrqpy

#Argument parser
parser = argparse.ArgumentParser(description="For the young pulsar timing paper")
parser.add_argument('-tnstrength',dest='tnstrength',help="Compute and plot correlation between sigma_tn and Period derivative of young and MSPs", action='store_true')
parser.add_argument('-timingplots', dest='timingplots', help="Plot the timing residual plots (after removing the noise model) for all the young pulsars", action='store_true')
parser.add_argument('-noisescatter', dest='noisescatter', help="Plot the Ared vs Slope scatter", action='store_true')
parser.add_argument('-posteriors', dest="posteriors", help="Plot the integrated posterior distributions of Ared and Slope", action='store_true')
parser.add_argument('-correlation',dest='correlationage',help="Plot the correlation of the timing noise with the pulsar parameters", action='store_true')
parser.add_argument('-table1', dest='table1', help="Print table1 in paper1",action='store_true')
parser.add_argument('-table2', dest='table2', help='Print table2 in paper1',action='store_true')
parser.add_argument('-pmtable', dest='pmtable', help='Print proper motion table in paper1',action='store_true')
parser.add_argument('-biplot', dest='biplot', help="Plot braking index plot", action='store_true')
args=parser.parse_args()


#Reading the best model text file
bestmodel_path = "/fred/oz002/aparthas/p574/p574_FullBayesian/bestmodels.txt"
bestmodel_BF3_path = "/fred/oz002/aparthas/p574/p574_FullBayesian/bestmodels_BF3.txt"
bestmodels = np.loadtxt(bestmodel_path,dtype=str)
bestmodels_bf3 = np.loadtxt(bestmodel_BF3_path,dtype=str)

def get_mname(model):
    """
    Routine to return modelname to be presented in the paper
    """

    if model == "results_basicparams":
        modelname = "NoSP"
    elif model == "results_bp_noise_cpl":
        modelname = "CPL"
    elif model == "results_bp_noise_cpl_f2":
        modelname = "CPL+F2"
    elif model == "results_bp_noise_cpl_f2_lfc":
        modelname = "CPL+LFC+F2"
    elif model == "results_bp_noise_cpl_f2_sin":
        modelname = "CPL+F2+SIN"
    elif model == "results_bp_noise_cpl_lfc":
        modelname = "CPL+LFC"
    elif model == "results_bp_noise_cpl_pm":
        modelname = "CPL+PM"
    elif model == "results_bp_noise_cpl_pm_f2":
        modelname = "CPL+PM+F2"
    elif model == "results_bp_noise_cpl_pm_f2_lfc":
        modelname = "CPL+PM+F2+LFC"
    elif model == "results_bp_noise_cpl_pm_f2_sin":
        modelname = "CPL+PM+F2+SIN"
    elif model == "results_bp_noise_cpl_pm_lfc":
        modelname = "CPL+PM+LFC"
    elif model == "results_bp_noise_cpl_pm_sin":
        modelname = "CPL+PM+SIN"
    elif model == "results_bp_noise_cpl_sin":
        modelname = "CPL+SIN"
    elif model == "results_bp_noise_pl":
        modelname = "PL"
    elif model == "results_bp_noise_pl_f2":
        modelname = "PL+F2"
    elif model == "results_bp_noise_pl_f2_lfc":
        modelname = "PL+F2+LFC"
    elif model == "results_bp_noise_pl_f2_sin":
        modelname = "PL+F2+SIN"
    elif model == "results_bp_noise_pl_lfc":
        modelname = "PL+LFC"
    elif model == "results_bp_noise_pl_pm_f2":
        modelname = "PL+PM+F2"
    elif model == "results_bp_noise_pl_pm_f2_lfc":
        modelname = "PL+PM+F2+LFC"
    elif model == "results_bp_noise_pl_pm_f2_sin":
        modelname = "PL+PM+F2+SIN"
    elif model == "results_bp_noise_pl_pm_lfc":
        modelname = "PL+PM+LFC"
    elif model == "results_bp_noise_pl_pm_sin":
        modelname = "PL+PM+SIN"
    elif model == "results_bp_noise_pl_pm_test":
        modelname = "PL+PM"
    elif model == "results_bp_noise_pl_sin":
        modelname = "PL+SIN"
    elif model == "results_bp_noise_pl_f2_extended":
        modelname = "PL+F2"
    elif model == "results_bp_noise_pl_extended":
        modelname = "PL"
    elif model == "results_bp_noise_pl_pm_f2_extended":
        modelname = "PL+PM+F2"
    elif model == "results_bp_noise_cpl_f2_lfc_extended1":
        modelname = "CPL+F2+LFC"
    return modelname


def get_position(posteriors):
    """
    Routine to obtain Bayesian values and confidence limits for RAJ and DECJ
    """

    upper_ra = np.percentile(posteriors["RAJ"],97.5) - np.percentile(posteriors["RAJ"],50)
    lower_ra = np.percentile(posteriors["RAJ"],50) - np.percentile(posteriors["RAJ"],2.5)
    mean_ra = np.percentile(posteriors["RAJ"],50)

    upper_dec = np.percentile(posteriors["DECJ"],97.5) - np.percentile(posteriors["DECJ"],50)
    lower_dec = np.percentile(posteriors["DECJ"],50) - np.percentile(posteriors["DECJ"],2.5)
    mean_dec = np.percentile(posteriors["DECJ"],50)

    
    return (mean_ra,upper_ra,lower_ra),(mean_dec,upper_dec,lower_dec)


def get_spin(posteriors):
    """
    Routine to obtain Bayesian values and confidence limits on F0 and F1
    """

    upper_f0 = np.percentile(posteriors["F0"],97.5) - np.percentile(posteriors["F0"],50)
    lower_f0 = np.percentile(posteriors["F0"],50) - np.percentile(posteriors["F0"],2.5)
    mean_f0 = np.percentile(posteriors["F0"],50)

    upper_f1 = np.percentile(posteriors["F1"],97.5) - np.percentile(posteriors["F1"],50)
    lower_f1 = np.percentile(posteriors["F1"],50) - np.percentile(posteriors["F1"],2.5)
    mean_f1 = np.percentile(posteriors["F1"],50)

    #multiplying f1 by 10^14 s^-2
    upper_f1 = 10**14 * upper_f1
    lower_f1 = 10**14 * lower_f1
    mean_f1 = 10**14 * mean_f1

    return(mean_f0,upper_f0,lower_f0),(mean_f1,upper_f1,lower_f1)


def get_pdot(f0,f1):
    """
    Routine to compute the Pdot given F0 and F1
    """

    pdot = np.log10(np.power(f0[0],-2)*np.power(abs(f1[0]),1))

    return pdot

def get_f2(posteriors):
    """
    Routine to obtain Bayesian values and confidence limits on F0 and F1
    """

    upper_f2 = np.percentile(posteriors["F2"],97.5) - np.percentile(posteriors["F2"],50)
    lower_f2 = np.percentile(posteriors["F2"],50) - np.percentile(posteriors["F2"],2.5)
    mean_f2 = np.percentile(posteriors["F2"],50)

    #Multiplying F2 by 10^23s^-2
    upper_f2 = 10**23 * upper_f2
    lower_f2 = 10**23 * lower_f2
    mean_f2 = 10**23 * mean_f2

    return(mean_f2,upper_f2,lower_f2)
 


def get_bindex(posteriors):
    """
    Routine to compute the Braking index from F0,F1 and F2
    """

    upper_f0 = np.percentile(posteriors["F0"],97.5)
    lower_f0 = np.percentile(posteriors["F0"],2.5)
    mean_f0 = np.percentile(posteriors["F0"],50)

    upper_f1 = np.percentile(posteriors["F1"],97.5)
    lower_f1 = np.percentile(posteriors["F1"],2.5)
    mean_f1 = np.percentile(posteriors["F1"],50)

    upper_f2 = np.percentile(posteriors["F2"],97.5)
    lower_f2 = np.percentile(posteriors["F2"],2.5)
    mean_f2 = np.percentile(posteriors["F2"],50)

    upper_brake = (upper_f0*upper_f2)/upper_f1**2 - (mean_f0*mean_f2)/mean_f1**2
    lower_brake = (mean_f0*mean_f2)/mean_f1**2 - (lower_f0*lower_f2)/lower_f1**2
    mean_brake = (mean_f0*mean_f2)/mean_f1**2


    #Rounding
    mean_brake = round(float(mean_brake),2)
    upper_brake = round(float(upper_brake),2)
    lower_brake = round(float(lower_brake),2)

    #print "BRAKING INDEX"
    #print "{0} {1} {2}".format(brake_mean,brake_upper, brake_lower) 
    return (mean_brake, upper_brake, lower_brake)

def frompar(results):
    """
    Routine to extract PEPOCH, NTOA MJDRANGE and TIMESPAN from the epehemeris file
    """

    parfile = glob.glob(os.path.join(results,"Red*.par"))[0]
    with open(parfile,"r") as f:
        parlines = f.readlines()
    f.close()
    for line in parlines:
        sline = line.split()
        if sline[0] == "PEPOCH":
            pepoch = int(sline[1])
        if sline[0] == "START":
            start = float(sline[1])
        if sline[0] == "FINISH":
            finish = float(sline[1])
            timespan = float((finish-start)/365.25)
        if sline[0] == "NTOA":
            ntoa = int(sline[1])

    #Rounding
    start = int(start)
    finish = int(finish)
    timespan = round(timespan,1)


    return (pepoch,start,finish,timespan,ntoa)


def get_noTNpar(results):
    """
    Routine to generate a new ephemeris file without the timing noise parameters
    """
    parfile = glob.glob(os.path.join(results,"Red*.par"))[0]
    with open(parfile,"r") as f:
        parlines = f.readlines()
    f.close()
    with open(os.path.join(results,"notimingnoise.par"),"w") as newf:
        for line in parlines:
            sline=line.split()
            if sline[0].startswith("TN"):
                pass
            else:
                newf.write(" ".join(sline))
                newf.write("\n")

    newf.close()

    newf_path = os.path.join(results,"notimingnoise.par")
    return newf_path


def get_residuals(noTNpar,tim):
    """
    Routine to create a residuals.dat file using tempo2.
    """

    path,name = os.path.split(noTNpar)
    if not os.path.exists(os.path.join(path,"noTN_residuals.dat")):
        args = "tempo2 -output general2 -s \"{{sat}} {{post}} {{err}} {{freq}}\n\" -f {0} {1} -outfile residuals.dat -nofit -fit F0 -fit F1".format(
                noTNpar,tim)
        proc = shlex.split(args)
        subprocess.call(proc)
        if os.path.isfile("residuals.dat"):
            os.rename('residuals.dat','{0}/noTN_residuals.dat'.format(path))
            print "{0}/noTN_residuals.dat file generated".format(path)

    else:
        print "{0}/noTN_residuals.dat exists".format(path)

    residuals = np.genfromtxt(os.path.join(path,"noTN_residuals.dat"))
    yerrors = [x * 10**-6 for x in residuals[:,2]]

    return residuals,yerrors

def get_bayesfactors(results):
    """
    Routine to get the Bayes factor relative to the PL model
    """

    path,name = os.path.split(results)
    pl_stats = glob.glob(os.path.join(path,"results_bp_noise_pl/*-stats.dat"))[0]
    with open(pl_stats) as f:
        for line in f:
            sline = line.split()
            if len(sline) > 0:
                if sline[1] == "Importance":
                    pl_evidence = round(float(sline[5]),2)
                    pl_evidence_err = round(float(sline[7]),2)
    f.close()
    best_stats = glob.glob(os.path.join(results,"*-stats.dat"))[0]
    with open(best_stats) as f:
        for line in f:
            sline = line.split()
            if len(sline) > 0:
                if sline[1] == "Importance":
                    best_evidence = round(float(sline[5]),2)
                    best_evidence_err = round(float(sline[7]),2)
    f.close()

    bayesfactor = best_evidence - pl_evidence

    return name,bayesfactor

def get_timingnoise(posteriors):
    """
    Routine to get the Bayesian estimates and confidence limits for the timing noise parameters
    """

    upper_amp = np.percentile(posteriors["RedAmp"],97.5) - np.percentile(posteriors["RedAmp"],2.5)
    lower_amp = np.percentile(posteriors["RedAmp"],50) - np.percentile(posteriors["RedAmp"],2.5)
    mean_amp = np.percentile(posteriors["RedAmp"],50)

    upper_slope = np.percentile(posteriors["RedSlope"],97.5) - np.percentile(posteriors["RedSlope"],2.5)
    lower_slope = np.percentile(posteriors["RedSlope"],50) - np.percentile(posteriors["RedSlope"],2.5)
    mean_slope = np.percentile(posteriors["RedSlope"],50)

    #A_{NG} = 10**(A_{TN})/sqrt{12 pi^2} * (365.25*24*3600*1e6)^2
    #print mean_amp, ((10**(mean_amp))*np.sqrt(12*np.pi*np.pi)) * (365.25*24*3600*1000)**2
    #print mean_amp, np.sqrt((10**mean_amp)*12*np.pi*np.pi * (24*60*60*1000000*365.25*10.54))

    #Rounding 
    upper_amp = round(float(upper_amp),2)
    lower_amp = round(float(lower_amp),2)
    mean_amp = round(float(mean_amp),2)

    upper_slope = round(float(upper_slope),2)
    lower_slope = round(float(lower_slope),2)
    mean_slope = round(float(mean_slope),2)

    return (mean_amp,upper_amp,lower_amp),(mean_slope,upper_slope,lower_slope)


def get_sigmaTN(amp,slope):
    """
    Routine to compute sigma_tn = amp+log10(T)*slope 
    """

    mean_sigmaTN = amp[0]+np.log10(10)*slope[0]
    upper_sigmaTN = amp[1]+np.log10(10)*slope[1]
    lower_sigmaTN = amp[2]+np.log10(10)*slope[2]

    return (mean_sigmaTN, upper_sigmaTN, lower_sigmaTN)



def get_corner(posteriors):
    """
    Routine to obtain estimates on the corner frequency
    """

    upper_corner = np.percentile(posteriors["RedCorner"],97.5)- np.percentile(posteriors["RedCorner"],2.5)
    lower_corner = np.percentile(posteriors["RedCorner"],50) - np.percentile(posteriors["RedCorner"],2.5)
    mean_corner = np.percentile(posteriors["RedCorner"],50)

    return (mean_corner,upper_corner,lower_corner)

def convert_hms(ra,dec):
    """
    Routine to convert radians of RAJ, DECJ to HMS with uncertainties
    """

    mean_ra = Angle(ra[0], u.radian).to_string(unit=u.hour, sep=':')
    upper_ra = Angle(ra[1], u.radian).to_string(unit=u.hour, sep=':')
    lower_ra = Angle(ra[2], u.radian).to_string(unit=u.hour, sep=':')

    mean_dec = Angle(dec[0], u.radian).to_string(unit=u.degree, sep=':')
    upper_dec = Angle(dec[1], u.radian).to_string(unit=u.degree, sep=':')
    lower_dec = Angle(dec[2], u.radian).to_string(unit=u.degree, sep=':')

    return (mean_ra,upper_ra,lower_ra),(mean_dec,upper_dec,lower_dec)



def plot_intposteriors(int_amp,int_slope,name):
    """
    Routine to plot the integrated posterior distributions for the Rednoise amplitude and slope for $name
    """

    print "Ploting the integrated posterior distribution for {0}".format(name)
    samples = [int_amp,int_slope]
    pars = [r"$\log_{\rm 10}(A_{\mathrm{red}})$", r"$\beta$"]

    c = ChainConsumer()
    c.add_chain(samples,parameters=pars,bins=40,name="{0}".format(name))
    c.configure(diagonal_tick_labels=False,flip=False,sigma2d=True,kde=False,sigmas=[1.19,2.5],shade=True,shade_alpha=0.1,label_font_size=20,tick_font_size=15,max_ticks=3,summary=True,colors="#2c3e50",plot_color_params=True,contour_labels="confidence",summary_area=0.95)
    c.plotter.plot(figsize=(9,9),extents={r"$\log_{\rm 10}(A_{\mathrm{red}})$":(-13,-8.4),r"$\beta$":(-12,1)})
    plt.savefig("forpaper/TN_posteriors.pdf")
    plt.show()

def get_pminfo(posteriors,psrname):
    """
    Routine to compute Total proper motion and transverse velocity 
    """

    upper_pmra = np.percentile(posteriors["PMRA"],97.5) - np.percentile(posteriors["PMRA"],50)
    lower_pmra = np.percentile(posteriors["PMRA"],50) - np.percentile(posteriors["PMRA"],2.5)
    mean_pmra = np.percentile(posteriors["PMRA"],50)

    upper_pmdec = np.percentile(posteriors["PMDEC"],97.5) - np.percentile(posteriors["PMDEC"],50)
    lower_pmdec = np.percentile(posteriors["PMDEC"],50) - np.percentile(posteriors["PMDEC"],2.5)
    mean_pmdec = np.percentile(posteriors["PMDEC"],50)

    upper_pmtot = math.sqrt(((mean_pmra*upper_pmra)**2 + (mean_pmdec*upper_pmdec)**2)/(mean_pmra**2 + mean_pmdec**2))
    lower_pmtot = math.sqrt(((mean_pmra*lower_pmra)**2 + (mean_pmdec*lower_pmdec)**2)/(mean_pmra**2 + mean_pmdec**2))
    mean_pmtot = math.sqrt(mean_pmra**2 + mean_pmdec**2)

    q = psrqpy.QueryATNF(params=['DIST_DM'], psrs=[str(psrname)])
    distance = float(q.dataframe['DIST_DM'])
    
    upper_transvel = float(4.74 * upper_pmtot * distance)
    lower_transvel = float(4.74 * lower_pmtot * distance)
    mean_transvel = float(4.74 * mean_pmtot * distance)
    
    upper_ratransvel = float(4.74 * upper_pmra * distance)
    lower_ratransvel = float(4.74 * lower_pmra * distance)
    mean_ratransvel = float(4.74 * mean_pmra * distance)
    
    #Rounding
    upper_pmra = round(float(upper_pmra),1)
    lower_pmra = round(float(lower_pmra),1)
    mean_pmra = round(float(mean_pmra),1)
    
    upper_pmdec = round(float(upper_pmdec),1)
    lower_pmdec = round(float(lower_pmdec),1)
    mean_pmdec = round(float(mean_pmdec),1)
    
    upper_pmtot = round(float(upper_pmtot),1)
    lower_pmtot = round(float(lower_pmtot),1)
    mean_pmtot = round(float(mean_pmtot),1)

    upper_transvel = round(float(upper_transvel),1)
    lower_transvel = round(float(lower_transvel),1)
    mean_transvel = round(float(mean_transvel),1)

    upper_ratransvel = round(float(upper_ratransvel),1)
    lower_ratransvel = round(float(lower_ratransvel),1)
    mean_ratransvel = round(float(mean_ratransvel),1)

    return (mean_pmra,upper_pmra,lower_pmra),(mean_pmdec,upper_pmdec,lower_pmdec),(mean_pmtot,upper_pmtot,lower_pmtot), distance, (mean_transvel,upper_transvel,lower_transvel),(mean_ratransvel,upper_ratransvel,lower_ratransvel)



def get_mspinfo(mean_sigmatn_all,upper_sigmatn_all,lower_sigmatn_all,pdot_all):
    """
    Routine to return sigma_TN and pdot for MSP from the IPTA DR1 sample
    """
    table = pd.read_csv("/fred/oz002/aparthas/p574/p574_pulsarfiles_newmodels1/p574_withMSPs.csv",comment="#")

    mean_sigmatn_msp =[]
    upper_sigmatn_msp=[]
    lower_sigmatn_msp=[]
    pdot_msp=[]

    msps = ["J2145-0750","J1713+0747","J0613-0200","J1024-0719","J1939+2134","J1824-2452A","J0621+1002","J1012+5307"]
    redamp_msps = ["-12.98","-14.0","-14.4","-13.9","-14.2","-12.73","-11.91","-13.18"]
    redamp_msps_err = ["0.05","0.2","0.5","0.2","0.2","0.19","0.08","0.09"]
    redslope_msps = ["0.6","3.1","5.0","5.4","6.0","3.0","1.9","1.5"]
    redslope_msps_err = ["0.2","0.6","1.0","0.6","0.5","1.0","0.3","0.3"]
   
    for num,name in enumerate(msps):
        #Timing noise
        redamp_mean = float(redamp_msps[num])
        redamp_error = float(redamp_msps_err[num])
        redslope_mean = float(redslope_msps[num])
        redslope_error = float(redslope_msps_err[num])

        mean_sigmatn_all.append(redamp_mean+np.log10(10)*redslope_mean)
        upper_sigmatn_all.append(redamp_error+np.log10(10)*redslope_error)
        lower_sigmatn_all.append(redamp_error+np.log10(10)*redslope_error)
        
        mean_sigmatn_msp.append(redamp_mean+np.log10(10)*redslope_mean)
        upper_sigmatn_msp.append(redamp_error+np.log10(10)*redslope_error)
        lower_sigmatn_msp.append(redamp_error+np.log10(10)*redslope_error)


        #Pdot
        index = table.index[table["Pulsar Name"] == str(name)].tolist()[0]
        f0_val = table["F0"].iloc[index]
        f1_val = abs(table["F1"].iloc[index])*10**14
        pdot_all.append(np.log10(np.power(f0_val,-2)*np.power(f1_val,1)))
        pdot_msp.append(np.log10(np.power(f0_val,-2)*np.power(f1_val,1)))


    return (mean_sigmatn_all, upper_sigmatn_all, lower_sigmatn_all),pdot_all,(mean_sigmatn_msp, upper_sigmatn_msp, lower_sigmatn_msp),pdot_msp


def plot_correlationage(f0_list,f1_list,sigmatn_orig):
    """
    Routine to compute the correlation of timing noise parameters with other pulsar parameters
    """

    alpha=[1.0]
    beta = np.arange(-2.0,2.0,0.1)
    f1_list = [abs(element) for element in f1_list]    
    for val_alpha in alpha:
        a =[]
        b =[]
        r =[]
        for val_beta in beta:
            fit_param = np.power(f0_list,val_alpha)*np.power(f1_list,val_beta)
            slope,intercept,r_value,p_value,std_err=stats.linregress(np.log10(fit_param),sigmatn_orig)
            a.append(round(val_alpha,1))
            b.append(round(val_beta,1))
            r.append(round(r_value,4))

            if round(val_beta,1) == -1.0:
                plt.errorbar(val_beta,r_value,fmt='o',color="#34495e",ecolor="#2980b9")
                plt.axvline(x=val_beta,linestyle='-.',color="#7f8c8d",label="Age",linewidth=2)
                print ("Age")
                print (r_value)

            elif round(val_beta,1) == 1.0:
                plt.errorbar(val_beta,r_value,color="#34495e",fmt='o',ecolor="#2980b9")
                plt.axvline(x=val_beta,linestyle='--',color="#7f8c8d",label=r'$\dot{E}$',linewidth=2)
                print ("Edot")
                print (r_value)

            elif round(val_beta,1) == -0.5:
                plt.errorbar(val_beta,r_value,color="#34495e",fmt='o',ecolor="#2980b9")
                plt.axvline(x=val_beta,linestyle=':',color="#7f8c8d",label=r'$\dot{P}$',linewidth=2)
                print ("Pdot")
                print (r_value)

            elif round(val_beta,1) == -0.3:
                plt.errorbar(val_beta,r_value,color="#34495e",fmt='o',ecolor="#2980b9")
                plt.axvline(x=val_beta,linestyle='--',color="#7f8c8d",label=r'$B_{surf}$',linewidth=2)
                print ("Bsurf")
                print (r_value)

            elif round(val_beta,1) == 0.3:
                plt.errorbar(val_beta,r_value,color="#34495e",fmt='o',ecolor="#2980b9")
                plt.axvline(x=val_beta,linestyle='-.',color="#7f8c8d",label=r'$B_{LC}$', linewidth=2)
                print ("BLC")
                print (r_value)   

        plt.plot(b,r,label=r'Young+MSPs'.format(val_alpha),color="#2980b9")        
        #Max correlation
        corr_alpha = a[r.index(min(r))]
        corr_beta = b[r.index(min(r))]
        corr = r[r.index(min(r))]
        print (corr_alpha,corr_beta,corr)
        plt.scatter(corr_beta,corr,color="#2c3e50",marker='x',s=10**2,label="Maximum correlation")

    plt.ylabel("Correlation Coefficient (r)",fontsize=30)
    plt.xlabel(r'$a$',fontsize=30)
    plt.legend(fontsize=18)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.savefig("forpaper/correlationage.pdf")
    plt.show()

"""
bestmodel[:,0] is the path to the pulsar directory
bestmodel[:,1] is the name of the best model directory
"""

#Results
TN_amp=[]
TN_slope=[]
pdot_list = []
mean_sigmatn = [] 
upper_sigmatn = []
lower_sigmatn = []
mean_amp = [] 
upper_amp = []
lower_amp = []
mean_slope = [] 
upper_slope = []
lower_slope = []
f0_list=[] #F0 for pulsars with timing noise
f1_list=[] #F1 for pulsars with timing noise
sigmatn_orig = [] #Sigma_TN values for young pulsars only
psrname_list = []

if args.biplot:
    style = dict(size=10,color='#34495e',rotation=90)
    style1 = dict(size=10,color='#2c3e50',rotation=90)
    plt.figure(figsize=(9,14))

if args.timingplots:
    fig,ax = plt.subplots(22,2,sharex=True,figsize=(15,17))
    font = {'family': 'serif',
            'color':  'white',
            'weight': 'normal',
            'size': 13,
            }
    num1=0
    num2=0

for num,psr in enumerate(bestmodels_bf3[:,0]):
    psrname = os.path.split(psr)[1]
    #print psrname
    psrname_list.append(psrname)
    results = os.path.join(psr,bestmodels_bf3[num,1])

    #Pulsars with a different model when BF>3 is assumed
    psrname_bf3 = ["J1512-5759","J1643-4505","J1715-3903","J1815-1738","J1820-1529"]
    if psrname in psrname_bf3:
        results = os.path.join(psr,bestmodels_bf3[num,1])

    #Getting parameter names for custom priors
    paramnames = glob.glob(os.path.join(results,"*.paramnames"))[0]
    names = pd.read_table(paramnames,delim_whitespace=True,names=("sno","param"),dtype={'sno': np.str, 'param': np.str})
    cnames = names['param'].tolist()
    cnames.append("Likelihood")

    #Getting the posterior values
    posterior_path = glob.glob(os.path.join(results,"*-post_equal_weights.dat"))[0]
    posteriors = pd.read_csv(posterior_path, names=cnames, delim_whitespace=True)


    #Rescaling the posteriors
    rescale_file = glob.glob(os.path.join(results,"*-T2scaling.txt"))[0]
    rescale_vals = pd.read_table(rescale_file,delim_whitespace=True,names=("param","units","mean","std"),dtype={'param':np.str, 'units':np.str, 'mean':np.str, 'std':np.str})
    for name in rescale_vals['param'].values:
        if name in cnames:
            posteriors[name] = float(rescale_vals.loc[rescale_vals['param'] == name, 'mean']) + float(rescale_vals.loc[rescale_vals['param'] == name, 'std'])*posteriors[name]
        else:
            pass

     
    #For Best model table
    model,bayesfactor = get_bayesfactors(results)
    modelname = get_mname(model)

    #For Timing solution table
    ra,dec = get_position(posteriors)
    ra,dec = convert_hms(ra,dec)

    f0,f1 = get_spin(posteriors)
    #Uncertainties on F0 and F1 using the upper uncertainty value
    f0_un = ufloat(f0[0],f0[1])
    f1_un = ufloat(f1[0],f1[1])

    #pepoch - start - finish - timespan - ntoa
    general_params = frompar(results)

    #For timing noise
    if "RedAmp" in cnames:
        amp,slope = get_timingnoise(posteriors)
        TN_amp.extend(posteriors["RedAmp"])
        TN_slope.extend(-posteriors["RedSlope"])
        mean_amp.append(amp[0])
        upper_amp.append(amp[1])
        lower_amp.append(amp[2])
        mean_slope.append(slope[0])
        upper_slope.append(slope[1])
        lower_slope.append(slope[2])
        sigma_tn = get_sigmaTN(amp,slope)
        mean_sigmatn.append(sigma_tn[0])
        sigmatn_orig.append(sigma_tn[0])
        upper_sigmatn.append(sigma_tn[1])
        lower_sigmatn.append(sigma_tn[2])
        pdot = get_pdot(f0,f1)
        pdot_list.append(pdot)
        f0_list.append(f0[0])
        f1_list.append(f1[0])
 



    if "RedCorner" in cnames:
        corner_freq = get_corner(posteriors)
        #TODO: Divide corner frequency by timespan
 

    if args.timingplots:
        #Generate par file from best model without the timing noise parameters
        notimingnoise_par = get_noTNpar(results)
        #Generate the residuals.dat file from tempo2 using the noTN par file and step1.tim file
        step1_tim = os.path.join(psr,"step1/step1.tim")
        noTN_residuals,yerrs = get_residuals(notimingnoise_par,step1_tim)
        #Plotting the timing residuals plots
        ax[num1,num2].errorbar(noTN_residuals[:,0], noTN_residuals[:,1], yerr=yerrs, ecolor='white',fmt='.', color='white',markersize=2)
        ax[num1,num2].set_title("{0}".format(psrname), x=1, y=0.5,  fontsize=8, color='white')
        ax[num1,num2].xaxis.set_major_locator(plt.MaxNLocator(3))
        ax[num1,num2].yaxis.set_major_locator(plt.MaxNLocator(2))
        #Customizing the subplots
        fig.patch.set_facecolor('black')
        ax[num1,num2].spines["top"].set_visible(False)
        ax[num1,num2].spines["right"].set_visible(False)
        ax[num1,num2].spines["bottom"].set_visible(False)
        ax[num1,num2].set_facecolor("black")
        ax[num1,num2].spines['left'].set_color('white')
        ax[num1,num2].tick_params(axis='x', colors='white',labelsize=10)
        ax[num1,num2].tick_params(axis='y', colors='white',labelsize=10)
        ax[num1,num2].yaxis.label.set_color('white')
        ax[num1,num2].xaxis.label.set_color('white')



        #Updating the subplot axes
        if num == 43:
            break

        print num,num1,num2
        if num1 == 21:
            num2+=1
            num1=0
        elif not num1 == 21:
            ax[num1,num2].tick_params(axis='x',bottom="off")
            num1+=1


    if args.table1:
        #LATEX TABLE
        #PSR - RAJ - DECJ - PEPOCH - F0 - F1 - NTOA - TIMESPAN - MJDRANGE 
        if "-" in psrname:
            psrname = psrname.split('-')[0]+"--"+psrname.split('-')[1]

        ra_mean = ra[0].split('.')[0]+"."+str((round(float("0."+ra[0].split('.')[1]),2))).split('.')[1]
        ra_upper = round(float("0."+ra[1].split('.')[1]),2)
        ra_lower = round(float("0."+ra[2].split('.')[1]),2)
        
        dec_mean = dec[0].split('.')[0]+"."+str((round(float("0."+dec[0].split('.')[1]),2))).split('.')[1]
        dec_upper = round(float("0."+dec[1].split('.')[1]),2)
        dec_lower = round(float("0."+dec[2].split('.')[1]),2)

        #if not abs(ra_upper - ra_lower) > 0.03:
        #    ra_un = ufloat(float("0."+ra[0].split('.')[1]),float(ra_upper))
        #    dec_un = ufloat(float("0."+dec[0].split('.')[1]),float(dec_upper))

        #    ra_mean = ra[0].split('.')[0]+'.'+str("{0:S}".format(ra_un)).split('.')[1]
        #    dec_mean = dec[0].split('.')[0]+'.'+str("{0:S}".format(dec_un)).split('.')[1]
        #    print "{0} & {1} & {2} & {3} & {4:S} & {5:S} & {6} & {7} & {8}-{9} \\\\".format(
        #        psrname,ra_mean,dec_mean,
        #        general_params[0], f0_un, f1_un, general_params[4], general_params[3], general_params[1], general_params[2])
        
        print "{0} & {{{1}}}$^{{{2}}}_{{{3}}}$ & {{{4}}}$^{{{5}}}_{{{6}}}$ & {7} & {8:S} & {9:S} & {10} & {11} & {12}-{13} \\\\".format(
            psrname,ra_mean,ra_upper,ra_lower,dec_mean,dec_upper,dec_lower,
            general_params[0], f0_un, f1_un, general_params[4], general_params[3], general_params[1], general_params[2])

    
 
    #Braking index and F2 
    if "F2" in cnames:
        brake = get_bindex(posteriors)
        f2 = get_f2(posteriors)
        f2_un = ufloat(round(f2[0],2),round(f2[1],2))
        brake_un = ufloat(round(brake[0],2),round(brake[1],2))
        brake_upper = round(brake[0]+brake[1],2)
        brake_lower = round(brake[0]-brake[2],2)
        if args.table2:
            #LATEX TABLE2
            #PSR - BESTMODEL - BAYESFACTOR - ARED - SLOPE - F2 - BRAKINGINDEX
            if "-" in psrname:
                psrname = psrname.split('-')[0]+"--"+psrname.split('-')[1]

            print "{0} & {1} & {2} & ${{{3}}}^{{{4}}}_{{{5}}}$ & ${{{6}}}^{{{7}}}_{{{8}}}$ & {9:.1uS} & {10:.1uS} \\\\".format(psrname,modelname,bayesfactor,round(amp[0],1),round(amp[1],1),round(amp[2],1), round(slope[0],1),round(slope[1],1),round(slope[2],1),f2_un,brake_un)

        if args.biplot:
            #Plotting the braking index confidence limts plot
            plt.errorbar(x=num, y=brake[0], yerr=[[brake_lower],[brake_upper]],fmt='o',color='#e74c3c')
            plt.text(num,brake[0]*5,str(psrname),weight='bold',**style1)

    else:
        #Choosing the F2+PL model for upper limits on F2 and braking index
        results1 = os.path.join(psr,"results_bp_noise_pl_f2")
        #Getting parameter names for custom priors
        paramnames1 = glob.glob(os.path.join(results1,"*.paramnames"))[0]
        names1 = pd.read_table(paramnames1,delim_whitespace=True,names=("sno","param"),dtype={'sno': np.str, 'param': np.str})
        cnames1 = names1['param'].tolist()
        cnames1.append("Likelihood")
        #Getting the posterior values
        posterior_path1 = glob.glob(os.path.join(results1,"*-post_equal_weights.dat"))[0]
        posteriors1 = pd.read_csv(posterior_path1, names=cnames1, delim_whitespace=True)
        #Rescaling the posteriors
        rescale_file1 = glob.glob(os.path.join(results1,"*-T2scaling.txt"))[0]
        rescale_vals1 = pd.read_table(rescale_file1,delim_whitespace=True,names=("param","units","mean","std"),dtype={'param':np.str, 'units':np.str, 'mean':np.str, 'std':np.str})
        for name in rescale_vals1['param'].values:
            posteriors1[name] = float(rescale_vals1.loc[rescale_vals1['param'] == name, 'mean']) + float(rescale_vals1.loc[rescale_vals1['param'] == name, 'std'])*posteriors1[name]
        
        #Upper limits on F2 and brake
        brake = get_bindex(posteriors1)
        f2 = get_f2(posteriors1)
        brake_upper = round(brake[0]+brake[1],2)
        brake_lower = round(brake[0]-brake[2],2)
        f2_upper = round(f2[0]+f2[1],2)
        f2_lower = round(f2[0]-f2[2],2)
        if args.table2:
            #LATEX TABLE2
            #PSR - BESTMODEL - BAYESFACTOR - ARED - SLOPE - F2 - BRAKINGINDEX
            if "-" in psrname:
                psrname = psrname.split('-')[0]+"--"+psrname.split('-')[1]

            print "{0} & {1} & {2} & ${{{3}}}^{{{4}}}_{{{5}}}$ & ${{{6}}}^{{{7}}}_{{{8}}}$ & ({9},{10}) & ({11},{12}) \\\\".format(psrname,modelname,bayesfactor,round(amp[0],1),round(amp[1],1),round(amp[2],1), round(slope[0],1),round(slope[1],1),round(slope[2],1),f2_upper,f2_lower,brake_upper,brake_lower)

        if args.biplot:
            #Plotting the braking index confidence limts plot
            plt.errorbar(x=num, y=abs(brake[0]),yerr=[abs(brake[0]-brake[2])],fmt='o',uplims=True,color="#3498db")
            plt.text(num,brake[1]*5,str(psrname),**style)
            #plt.errorbar(x=num, y=abs(brake[0]), yerr=[[abs(brake_lower)],[abs(brake_upper)]],fmt='o',color='#9b59b6')
            #plt.text(num,brake[0]*5,str(psrname),**style)




    if "PMRA" in cnames:
        if args.pmtable:
            pmra,pmdec,pmtot,distance,transvel,ra_transvel = get_pminfo(posteriors,psrname)
            #LATEX TABLE - PMRA
            #NAME - PMRA - PMDEC - PMTOT - DISTANCE - RA_TRANSVEL - TOT_TRANSVEL
            if "-" in psrname:
                psrname = psrname.split('-')[0]+"--"+psrname.split('-')[1]

            print "{0} & ${{{1}}}^{{{2}}}_{{{3}}}$ & ${{{4}}}^{{{5}}}_{{{6}}}$ & ${{{7}}}^{{{8}}}_{{{9}}}$ & {10} & ${{{11}}}^{{{12}}}_{{{13}}} & ${{{14}}}^{{{15}}}_{{{16}}}$ \\\\$".format(psrname, pmra[0],pmra[1],pmra[2], pmdec[0],pmdec[1],pmdec[2], pmtot[0],pmtot[1],pmtot[2],distance,ra_transvel[0],ra_transvel[1],ra_transvel[2],transvel[0],transvel[1],transvel[2])

#########################################
#PLOTS
if args.tnstrength:
    plt.errorbar(pdot_list,mean_sigmatn,yerr=[upper_sigmatn,lower_sigmatn],fmt='o',color="#27ae60",label="Young pulsars")
    sigmatn_all,pdot_all,sigmatn_msp,pdot_msp = get_mspinfo(mean_sigmatn,upper_sigmatn,lower_sigmatn,pdot_list)
    plt.errorbar(pdot_msp,sigmatn_msp[0],yerr=[sigmatn_msp[1],sigmatn_msp[2]],fmt='o',color="#3498db",label="IPTA DR1 - Millisecond pulsars")
    plt.plot(np.unique(pdot_all), np.poly1d(np.polyfit(pdot_all, sigmatn_all[0], 1))(np.unique(pdot_all)),color="#2c3e50")
    plt.xlabel(r"$\sigma_{\rm P} (\nu^{\rm -0.6}\dot{\nu}^{\rm 1})$", fontsize=35)
    plt.ylabel(r"$\sigma_{\rm TN}$",fontsize=35)
    plt.xticks(fontsize=20)
    plt.yticks(fontsize=20)
    plt.legend(fontsize=30,loc="upper left")
    plt.savefig("forpaper/TNstrength.pdf")
    plt.show()   

if args.correlationage:
    plot_correlationage(f0_list,f1_list,sigmatn_orig)

if args.noisescatter:
    #Generating the plot of the scatter plot of timing noise parameters - Amp vs slope
    print "Plotting the scatter plot - Amp vs Slope"
    plt.xlabel(r"$\log_{\rm 10}(A_{\mathrm{red}}$)",fontsize=25)
    plt.ylabel(r"$\beta$",fontsize=25)
    plt.xticks(fontsize=15)
    plt.yticks(fontsize=15)
    plt.errorbar(mean_amp, mean_slope, xerr=[upper_amp,lower_amp], yerr=[upper_slope, lower_slope],fmt='o',color="#2c3e50")
    plt.savefig("forpaper/TN_scatter.pdf")
    plt.show()

if args.timingplots:
    print "Plotting the timing residual plots"
    fig.text(0.5, 0.04, 'MJD', ha='center', va='center', fontdict=font)
    fig.text(0.03, 0.5, 'Timing Residuals (s)', ha='center', va='center', rotation='vertical', fontdict=font)
    plt.subplots_adjust(wspace=0.3,hspace=0.6)
    plt.subplots_adjust(left=0.12, right=0.9, top=0.95, bottom=0.07)
    plt.savefig("forpaper/Timingplots.pdf")
    plt.show()

if args.posteriors:
    plot_intposteriors(TN_amp,TN_slope,"TempoNest")

if args.biplot:
    plt.yscale("log")
    plt.xlabel("Pulsar Number",fontsize=25)
    plt.ylabel("Braking Index (n)",fontsize=25)
    plt.xticks(np.arange(0, len(psrname_list), 10),fontsize=18)
    plt.yticks(fontsize=18)

    plt.rc('font',family='serif',serif='Times')
    plt.rc('text', usetex=True)
    plt.rc('ytick', labelsize=15)
    plt.rc('axes', labelsize=15)

    plt.ylim(0.003,10000)
    plt.show()





