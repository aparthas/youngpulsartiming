"""
BayesTiming: Performs the full Bayesian analysis for each pulsar. Uses the Slurm scheduler.

__author__ = "Aditya Parthasarathy"
__copyright__ = "Copyright 2019, YoungPulsarTiming"
__license__ = "Public Domain"
__version__ = "0.2"
__maintainer__ = "Aditya Parthasarathy"
__email__ = "adityapartha3112@gmail.com"
__status__ = "Development"

"""


#IMPORTS
import argparse
import numpy as np
import sys
import glob
import os
import subprocess
import shlex
import time


parser=argparse.ArgumentParser(description="TempoNest pulsar timer")
parser.add_argument("-sim",dest="simulation",help="Flag to use simulated ToAs",action='store_true')
args=parser.parse_args()


#===========================================
#ANALYSIS ROUTINES
#===========================================

#Read and modify the input timing ephemeris
def modify_input_par(par,tim):
    """
    par: Path to step1.par
    tim: Path to step1.tim
    """
    parfile = open(par,"r")
    parlines = parfile.readlines()
    parfile.close()
    parfile = open(par,"w")
    for line in parlines:
        sline = line.split()
        if sline[0] == "START":
            parfile.write("START {0}\n".format(sline[1]))
        elif sline[0] == "FINISH":
            parfile.write("FINISH {0}\n".format(sline[1]))
        elif sline[0] == "JUMP":
            pass
        else:
            parfile.write(' '.join(sline))
            parfile.write("\n")
    parfile.close()

    print ("{0} file modified".format(par))
    return par,tim 


#Add parameters to the input timing ephemeris
def add_input_par(pulsar,par,tim,name,mode):
    """
    pulsar: Path to the pulsar directory
    par: Path to the modified step1.par
    tim: Path to the modified step1.tim
    name: Name of the new par file
    mode: What to add - PM or F2
    """

    parfile = open(par,"r")
    parlines = parfile.readlines()
    parfile.close()

    newpar = os.path.join(pulsar,name)

    if not os.path.exists(newpar):

        parfile = open(newpar,"w")
    
        if mode == "PM":
            for line in parlines:
                sline = line.split()
                if sline[0] == "F1":
                    parfile.write(' '.join(sline))
                    parfile.write("\n")
                    parfile.write("PMRA 0 1 \n")
                    parfile.write("PMDEC 0 1 \n")
                else:
                    parfile.write(' '.join(sline))
                    parfile.write("\n")

        elif mode == "F2":
            for line in parlines:
                sline = line.split()
                if sline[0] == "F1":
                    parfile.write(' '.join(sline))
                    parfile.write("\n")
                    parfile.write("F2 0 1 \n")
                else:
                    parfile.write(' '.join(sline))
                    parfile.write("\n")

        elif mode == "PM+F2":
            for line in parlines:
                sline = line.split()
                if sline[0] == "F1":
                    parfile.write(' '.join(sline))
                    parfile.write("\n")
                    parfile.write("PMRA 0 1 \n")
                    parfile.write("PMDEC 0 1 \n")
                    parfile.write("F2 0 1 \n")
                else:
                    parfile.write(' '.join(sline))
                    parfile.write("\n")

        
        parfile.close()
        print ("{0} ephermeris created.".format(newpar))
    else:
        print ("{0} already exists".format(newpar))

    return newpar,tim

   


#TempoNest: Fit for RAJ,DECJ,F0 and F1 as custom priors (simplest model)
#No white and red noise modelling
def temponest(pulsar,par,tim,cfile):
    """
    pulsar: Full path to the pulsar directory
    par: Path to step1.par
    tim: Path to step1.tim
    cfile: Path to basicparams.cfile
    """

    #Identifying the directory name from the input config file
    config_file = open(cfile,"r")
    cfile_lines = config_file.readlines()
    config_file.close()
    for line in cfile_lines:
        sline = line.split()
        if len(sline) > 0:
            if sline[0] == "root":
                dirname = sline[2].split("/")[0]

    #Creating the TN results directory
    psrpath,psrname = os.path.split(pulsar)
    if not os.path.exists(os.path.join(pulsar,dirname)):
        print ("Creating the TN results : {0}".format(dirname))
        os.makedirs(os.path.join(pulsar,dirname))
    tndir = os.path.join(pulsar,dirname)

    #Creating the bash script for the job
    job_name = str(psrname)+"_"+"{0}.bash".format(dirname)
    if not os.path.exists(os.path.join(tndir,job_name)):
        with open(os.path.join(tndir,job_name),'w') as job_file:
           
            job_file.write("#!/bin/bash \n")
            job_file.write("#SBATCH --job-name={0}\n".format(job_name))
            job_file.write("#SBATCH --output={0}/{1}.out \n".format(tndir,job_name))
            job_file.write("#SBATCH --ntasks=1 \n")
            job_file.write("#SBATCH --time=10:00:00 \n")
            job_file.write("#SBATCH --mem-per-cpu=3g \n")
            job_file.write('cd {0} \n'.format(pulsar))
            job_file.write("tempo2 -gr temponest -f {0} {1} -cfile {2}".format(par,tim,cfile))
            job_file.close()

    #Deploying the job
    if not os.path.exists(os.path.join(tndir,"{0}.out".format(job_name))):
        com_sbatch = 'sbatch {0}/{1}'.format(tndir,job_name)
        args_sbatch = shlex.split(com_sbatch)
        proc_sbatch = subprocess.Popen(args_sbatch)
        print ("{0} deployed".format(job_name))



#==================================
#DEFINE INPUT AND ANALYSIS PIPELINE
#==================================
#INPUT PATH
inputpath = "/fred/oz002/aparthas/p574/p574_FullBayesian"
config_path = "/fred/oz002/aparthas/p574/p574_FullBayesian/config_files"
config_roster = "/fred/oz002/aparthas/p574/p574_FullBayesian/config_files/config_roster.txt"

pulsars = sorted(glob.glob(os.path.join(inputpath,"J*")))

for pulsar in pulsars:
    psrpath,psrname = os.path.split(pulsar)
    print "######################################### {0} ###################################################".format(psrname)
    par = os.path.join(pulsar,"step1/step1.par")
    if args.simulation:
        print ("Using simulated ToAs")
        tim = os.path.join(pulsar,"step1/step1_simulated.tim")
    else:
        tim = os.path.join(pulsar,"step1/step1.tim")

    if os.path.exists(par):
        if os.path.exists(tim):
            
            par,tim = modify_input_par(par,tim)

            #Reading the config roster to decide the input ephemeris and the config file
            croster = open(config_roster,"r")
            croster_lines = croster.readlines()
            croster.close()
            for line in croster_lines:
                sline = line.split()
                if len(sline) > 0:
                    if not sline[0] == "#":
                        model = sline[0].split("+")
                        cfile = os.path.join(config_path,sline[1])

                        if "onlyPM" in model:
                            mode = "PM"
                            name = "{0}_PM.par".format(psrname)
                            npar,ntim=add_input_par(pulsar,par,tim,name,mode)
                            temponest(pulsar,npar,ntim,cfile)

                        elif "onlyF2" in model:
                            mode = "F2"
                            name = "{0}_F2.par".format(psrname)
                            npar,ntim=add_input_par(pulsar,par,tim,name,mode)
                            temponest(pulsar,npar,ntim,cfile)

                        elif "PM" in model:
                            mode = "PM+F2"
                            name = "{0}_PM_F2.par".format(psrname)
                            npar,ntim=add_input_par(pulsar,par,tim,name,mode)
                            temponest(pulsar,npar,ntim,cfile)

                        else:
                            temponest(pulsar,par,tim,cfile)
        else:
            print ("{0} file does not exist".format(tim))
    else:
        print ("{0} file does not exist".format(par))



